#!/bin/bash

echo "Please enter the username:"
read USERNAME

echo "...and now your API KEY:"
read APIKEY

TOKEN=$(
curl -sd \
"{
   \"auth\":
   {
        \"RAX-KSKEY:apiKeyCredentials\":
        {\"username\": \"$USERNAME\",
        \"apiKey\": \"$APIKEY\"}        }
}" \
-H 'Content-Type: application/json' \
'https://identity.api.rackspacecloud.com/v2.0/tokens' | python -m json.tool | grep -A 5 token | awk '/id/ { print $2 }' | tr -d '"' | tr -d ","
) 

export TOKEN=$TOKEN
echo "Your token $TOKEN has been exported."
#TOKEN=$(echo "{\"auth\":{\"RAX-KSKEY:apiKeyCredentials\":{\"username\":\"$USERNAME\", \"apiKey\":\"$APIKEY\"}}}" | http --json POST https://identity.api.rackspacecloud.com/v2.0/tokens |  python -m json.tool | grep -A 5 token | awk '/id/ { print $2 }' | tr -d '"' | tr -d ",")



